package com.gavrilovmiroslav.awsom.examples.words;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import com.gavrilovmiroslav.awsom.InputFactory;
import com.gavrilovmiroslav.awsom.LabelFactory;
import com.gavrilovmiroslav.awsom.SelfOrganizingMap;
import com.gavrilovmiroslav.awsom.SelfOrganizingMap.DiffPair;
import com.gavrilovmiroslav.awsom.SelfOrganizingMap.PresetInputFactory;

public class Main {
	public static void main(String[] args) {
		new Main();
	}
	
	public Main() {		
		// setting up starting values

		final InputFactory<WordVector> wordFactory = () -> {
			return WordVector.createRandom();
		};

		final LabelFactory<String> stringFactory = () -> {
			return "";
		};

		// create map

		final SelfOrganizingMap<WordVector, String> map = new SelfOrganizingMap<>(
				wordFactory, stringFactory);

		// define file format for data sets

		final PresetInputFactory<SelfOrganizingMap<WordVector, String>.Node> wordParser = line -> {
			return map.new Node(WordVector.createSpecific(line), line);
		};

		// create view and connect it to the map

		/*
		 * final ColorView view = new ColorView(); final boolean[] added = new
		 * boolean[] { false };
		 * 
		 * final Runnable updateView = () -> { view.setTitle("" + map.age + "/"
		 * + map.maxAge + " with " + map.dataSet.size() + " entries.");
		 * view.updateMap(map); };
		 * 
		 * // define a click event and load new dataset on click -- even if the
		 * old // set is being studied
		 * 
		 * view.addMouseListener(new MouseAdapter() {
		 * 
		 * @Override public void mouseClicked(MouseEvent e) { if (!added[0]) {
		 * map.loadDataSetFrom("testing.set", colorParser); added[0] = true; } }
		 * });
		 * 
		 * map.onFinishedOneStep = updateView; map.onFinishedWholeDataSet =
		 * updateView; map.onLabellingFinished = updateView;
		 */
		// enter the training set

		map.loadDataSetFrom("words.set", wordParser);
		
		HashMap<String, Integer> positions = new HashMap<String, Integer>();
		Iterator<Entry<String, SelfOrganizingMap<WordVector, String>.DiffPair>> it = map.getLabels();
		
		while(it.hasNext()) {
			Entry<String, SelfOrganizingMap<WordVector, String>.DiffPair> entry = it.next();
			positions.put(entry.getKey(), entry.getValue().index);
		}
		
		map.loadDataSetFrom("similar.set", wordParser);

		it = map.getLabels();
		
		int[] a = new int[] { 0, 0 }, b = new int[] { 0, 0 }, d = new int[] { 0, 0 };
		
		while(it.hasNext()) {
			Entry<String, SelfOrganizingMap<WordVector, String>.DiffPair> entry = it.next();
			if(positions.containsKey(entry.getKey())) continue;
			
			int newWordIndex = entry.getValue().index;
			map.getMatrixIndice(a, newWordIndex);
			
			double minDistance = Double.MAX_VALUE;
			String bestMatch = "";
			
			for(String old : positions.keySet()) {
				int oldWordIndex = positions.get(old);
				map.getMatrixIndice(b, oldWordIndex);
				
				d[0] = a[0] - b[0]; 
				d[1] = a[1] - b[1];
				
				double distance = Math.sqrt(d[0] * d[0] + d[1] * d[1]);
				if(distance < minDistance) {
					minDistance = distance;
					bestMatch = old;
				}
			}
			
			System.out.format("Best match for %s is %s.\n", entry.getKey(), bestMatch);
		}
	}
}
