package com.gavrilovmiroslav.awsom;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/*
 * Self Organizing Map structure
 * 
 * 
 * 						 Node -  Node ... Node
 *                        |   x   |    x   |
 *                       Node -  Node ... Node
 *                        |   x   |    x   |
 *                       Node -  Node ... Node               
 *                        |   x   |    x   |
 * Sensor -- input -> {  Node -  Node ... Node  } -- output -> Label
 *                        |   x   |    x   |
 *                       Node -  Node ... Node
 *                        |   x   |    x   |
 *                       Node -  Node ... Node               
 *                        |   x   |    x   |
 * 						 Node -  Node ... Node
 * 
 * Input = some vector of data with a label
 * Label = any information we use to distinguish the data
 * 
 * Training:
 * 1. Input finds the Node with the most similar value as itself = BMN (Best Matching Node)
 * 2. Fix BMN and its r-environment (reciprocal age) to be more similar to the input 
 * 3. Repeat for different inputs and increase age (decrease r-environment)
 * 4. After aging beyond maximum age, label the hot-spots for the training set
 * 5. Regenerate to an earlier age, so that more learning could be done
 * 
 */
public class SelfOrganizingMap<Input extends Vector<Input>, Label> {
	public interface PresetInputFactory<InputType> {
		public InputType create(String s);
	}

	public class Node {
		public Input value;
		public Label label;

		public Node(Input v, Label l) {
			this.value = v;
			this.label = l;
		}
	}

	public class DiffPair {
		public int index;
		double diff;

		public DiffPair(int i, double d) {
			this.index = i;
			this.diff = d;
		}
	}

	public boolean debug;
	private Logger log;

	public ArrayList<Node> map;

	public ArrayList<Node> dataSet;
	public Label output;

	public int size;

	private double maxFieldOfInfluence;
	public double age;
	public double maxAge;

	private double timeConstant;

	public double learningRate;
	public double startingLearningRate;

	public double ageRegenerationFactor = 10;
	public double learningRegenerationFactor = 100;

	public Runnable onFinishedOneInput = null;
	public Runnable onFinishedOneStep = null;
	public Runnable onFinishedWholeDataSet = null;
	public Runnable onLabellingFinished = null;

	public int pauseBetweenInputs = 100;
	public int pauseBetweenSteps = 100;
	public int pauseAfterDataset = 100;
	public int pauseAfterLabelling = 100;

	private boolean isLearning = false;
	
	private volatile HashMap<Label, DiffPair> labels = new HashMap<>();
	private ArrayList<Node> toBeLoaded = new ArrayList<>();
	
	private void writeLog(String message) {
		if (this.debug)
			this.log.info(message);
	}

	public SelfOrganizingMap(InputFactory<Input> inputFactory,
			LabelFactory<Label> labelFactory) {

		this.log = Logger.getLogger("DEBUG");

		if (this.debug)
			try {
				final FileHandler fh = new FileHandler(new SimpleDateFormat(
						"yyyy-MM-dd.HH-mm-ss").format(new Date()) + ".log");
				this.log.addHandler(fh);
				SimpleFormatter formatter = new SimpleFormatter();
				fh.setFormatter(formatter);
			} catch (SecurityException | IOException e) {
				e.printStackTrace();
			}

		this.dataSet = new ArrayList<>();

		this.age = 0;

		this.loadConfiguration();

		this.map = new ArrayList<>();

		for (int i = 0; i < this.size * this.size; i++) {
			this.map.add(new Node(inputFactory.create(), labelFactory.create()));
		}

		this.maxFieldOfInfluence = 3 / 2 * this.size * Math.sqrt(2);

		this.timeConstant = this.maxAge / Math.log(this.maxFieldOfInfluence);
	}

	public void loadDataSetFrom(String filename,
			PresetInputFactory<Node> inputVectorFactory) {
		BufferedReader set = null;

		try {
			set = new BufferedReader(new InputStreamReader(new FileInputStream(
					filename)));
			String line;
			synchronized (this.toBeLoaded) {
				while ((line = set.readLine()) != null) {
					this.toBeLoaded.add(inputVectorFactory.create(line));
				}
			}
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (set != null)
				try {
					set.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
				
		if(!this.isLearning) {
			this.train();
			this.label();
		}
	}

	private void regenerate() {
		if(!this.toBeLoaded.isEmpty()) {
			for(final Node node : this.toBeLoaded) {
				this.dataSet.add(node);
			}
			
			this.toBeLoaded.clear();
			
			this.age = Math.min(this.age, (int) ((double) this.maxAge
					* this.ageRegenerationFactor / 100.0));
			
			this.learningRate = Math.min(this.learningRate,
					this.startingLearningRate
							* this.learningRegenerationFactor / 100.0);

			writeLog("Regenerated to " + this.ageRegenerationFactor
					+ "% age and " + this.learningRegenerationFactor
					+ "% learning ability.");				
		}
	}
	
	public void train() {
		this.isLearning = true;
		writeLog("Training started: " + this.age + "/" + this.maxAge);

		regenerate();
		
		while(this.age < this.maxAge) {
			regenerate();
			
			final int[] matrixBMU = new int[] { 0, 0 };
			double xa, xb, ya, yb, x, y;

			Collections.shuffle(this.dataSet);

			final double neighborhoodRadius = this.maxFieldOfInfluence
					* Math.exp(-(double) this.age / this.timeConstant);

			this.learningRate = this.startingLearningRate
					* Math.exp(-((double) this.age) / (double) this.maxAge);

			writeLog("===============================================================================");
			writeLog("Training -- age: " + this.age + "/" + this.maxAge
					+ ", age factor: " + neighborhoodRadius
					+ ", learning rate: " + this.learningRate);
			writeLog("===============================================================================");

			Iterator<Node> it = this.dataSet.iterator();
			
			while (it.hasNext()) {
				final Node inputNode = it.next();

				Input input = inputNode.value;
				writeLog("Training -- input: " + input.toString() + " at age " + this.age + "/" + this.maxAge);

				final DiffPair bmu = this.findBestMatch(input);

				getMatrixIndice(matrixBMU, bmu.index);
				writeLog("Found best matching unit at " + matrixBMU[0]
						+ ", " + matrixBMU[1] + " with deviation of "
						+ bmu.diff + ".");

				x = matrixBMU[0];
				y = matrixBMU[1];

				xa = Math.max(0, x - neighborhoodRadius);
				ya = Math.max(0, y - neighborhoodRadius);

				xb = Math.min(x + neighborhoodRadius, this.size);
				yb = Math.min(y + neighborhoodRadius, this.size);

				int count = 0;
				for (double dx = xa; dx < xb; dx++) {
					for (double dy = ya; dy < yb; dy++) {
						count += this.updateMap(matrixBMU, (int) dx,
								(int) dy, neighborhoodRadius, input) ? 1
								: 0;
					}
				}

				if (count == 0) {
					count += this.updateMap(matrixBMU, matrixBMU[0],
							matrixBMU[1], neighborhoodRadius, input) ? 1
							: 0;
				}

				writeLog("Updated " + count + " nodes with this input.");

				if (this.onFinishedOneInput != null) {
					this.onFinishedOneInput.run();
				}

				try {
					Thread.sleep(this.pauseBetweenInputs);
				} catch (Exception e) {				
				}
				Thread.yield();
			}

			if (this.onFinishedOneStep != null) {
				this.onFinishedOneStep.run();
			}

			try {
				Thread.sleep(this.pauseBetweenSteps);
			} catch (Exception e) {
			}
			Thread.yield();
			
			this.age++;
		}

		this.isLearning = false;

		if (this.onFinishedWholeDataSet != null) {
			this.onFinishedWholeDataSet.run();
		}
		
		try {
			Thread.sleep(this.pauseAfterDataset);
		} catch (Exception e) {
		}		
	}

	public void label() {
		int x, y;
		int[] matrixBMU = new int[] { 0, 0 };

		for (Node inputNode : this.dataSet) {
			Input input = inputNode.value;
			final DiffPair bmu = this.findBestMatch(input);

			getMatrixIndice(matrixBMU, bmu.index);
			this.map.get(bmu.index).label = inputNode.label;

			this.labels.put(inputNode.label, new DiffPair(bmu.index, 0));

			x = matrixBMU[0];
			y = matrixBMU[1];

			writeLog("Labelling -- input: " + input.toString() + " ("
					+ inputNode.label + ") to node " + x + ", " + y);
		}

		if (this.onLabellingFinished != null) {
			this.onLabellingFinished.run();
		}

		try {
			Thread.sleep(this.pauseAfterLabelling);
		} catch (Exception e) {
		}
	}

	public synchronized Iterator<Entry<Label, DiffPair>> getLabels() {
		return this.labels.entrySet().iterator();
	}

	private boolean updateMap(int[] bmu, int x, int y, double radius,
			Input input) {
		double distance = Math.sqrt((bmu[0] - x) * (bmu[0] - x) + (bmu[1] - y)
				* (bmu[1] - y));

		int[] xy = new int[] { x, y };

		final int updateIndex = this.getArrayIndex(xy);
		final Node updatedNode = this.map.get(updateIndex);

		if (distance <= radius) {
			double influenceOfBMU = Math.exp(-(distance * distance)
					/ (2 * radius * radius));
			updatedNode.value.assimilate(input, influenceOfBMU
					* this.learningRate);
			this.map.set(updateIndex, updatedNode);
			return true;
		}

		return false;
	}

	private DiffPair findBestMatch(Input input) {
		int minIndex = 0;
		double minDiff = Double.MAX_VALUE;

		for (int i = 0; i < this.map.size(); i++) {
			double diff = this.map.get(i).value.diff(input);
			if (diff < minDiff) {
				minDiff = diff;
				minIndex = i;
			}
		}

		return new DiffPair(minIndex, minDiff);
	}

	private void loadConfiguration() {
		ScriptEngineManager engineManager = new ScriptEngineManager();
		ScriptEngine engine = engineManager.getEngineByName("nashorn");
		try {
			engine.put("net", this);
			engine.eval(new BufferedReader(new InputStreamReader(
					new FileInputStream("config.properties"))));
		} catch (FileNotFoundException | ScriptException e) {
			e.printStackTrace();
		}
	}

	public void getMatrixIndice(int[] matrixIndice, int arrayIndex) {
		matrixIndice[0] = arrayIndex / this.size;
		matrixIndice[1] = arrayIndex % this.size;
	}

	public int getArrayIndex(int[] matrixIndice) {
		return matrixIndice[0] * this.size + matrixIndice[1];
	}
}
