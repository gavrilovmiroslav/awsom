package com.gavrilovmiroslav.awsom;

public abstract class Vector<Other extends Vector<Other>> {
	public abstract double diff(Other v);
	public abstract void assimilate(Other v, double strength);
}
