package com.gavrilovmiroslav.awsom.examples.colors;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.Iterator;
import java.util.Map.Entry;

import javax.swing.JFrame;
import javax.swing.JPanel;

import com.gavrilovmiroslav.awsom.SelfOrganizingMap;

public class ColorView extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public class ColorViewPanel extends JPanel {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		SelfOrganizingMap<ColorVector, String> map;

		public ColorViewPanel() {
		}

		public void setMap(SelfOrganizingMap<ColorVector, String> map) {
			this.map = map;
		}

		public void paint(Graphics _g) {
			if (map == null || map.map == null)
				return;

			final Graphics2D g = (Graphics2D) _g;

			int i = 0, j = 0, c = 0;
			int offsetX = 100;
			int offsetY = 100;

			g.setColor(Color.white);			
			g.fillRect(0, 0, this.getWidth(), this.getHeight());
			
			g.translate(offsetX, offsetY);

			for (SelfOrganizingMap<ColorVector, String>.Node color : map.map) {
				i = c / this.map.size;
				j = c % this.map.size;

				final Color pixel = color.value.toRGBColor();
				g.setColor(pixel);
				g.fillRect(5 * i, 5 * j, 5, 5);

				c++;
			}

			Iterator<Entry<String, SelfOrganizingMap<ColorVector, String>.DiffPair>> it = map
					.getLabels();

			Entry<String, SelfOrganizingMap<ColorVector, String>.DiffPair> entry = null;

			while (it.hasNext()) {
				entry = it.next();

				final int key = entry.getValue().index;
				final String str = " " + entry.getKey() + " ";

				int x = 5 * (key / map.size);
				int y = 5 * (key % map.size);

				FontMetrics fm = g.getFontMetrics();
				Rectangle2D rect = fm.getStringBounds(str, g);

				g.setColor(Color.gray);
				g.fillRect(x, y - fm.getAscent(), (int) rect.getWidth(),
						(int) rect.getHeight());

				g.setColor(Color.white);
				g.drawString(str, x, y);
			}
		}
	}

	private ColorViewPanel panel = new ColorViewPanel();

	public ColorView() {
		this.setContentPane(panel);
		this.setSize(800, 800);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}

	public void updateMap(SelfOrganizingMap<ColorVector, String> map) {
		((ColorViewPanel) this.getContentPane()).setMap(map);
		((ColorViewPanel) this.getContentPane()).repaint();
	}
}
