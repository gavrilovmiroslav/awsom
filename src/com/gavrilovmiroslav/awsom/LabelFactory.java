package com.gavrilovmiroslav.awsom;

public interface LabelFactory<T> {
	public T create();
}
