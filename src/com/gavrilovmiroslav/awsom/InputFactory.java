package com.gavrilovmiroslav.awsom;

public interface InputFactory<Input> {	
	public Input create();
}
