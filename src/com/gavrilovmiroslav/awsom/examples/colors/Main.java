package com.gavrilovmiroslav.awsom.examples.colors;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import com.gavrilovmiroslav.awsom.InputFactory;
import com.gavrilovmiroslav.awsom.LabelFactory;
import com.gavrilovmiroslav.awsom.SelfOrganizingMap;
import com.gavrilovmiroslav.awsom.SelfOrganizingMap.PresetInputFactory;

public class Main {
	public static void main(String[] args) {
		new Main();
	}

	public Main() {
		// setting up starting values
		
		final InputFactory<ColorVector> colorFactory = () -> {
			return new ColorVector(Math.random(), Math.random(), Math.random());
		};

		final LabelFactory<String> stringFactory = () -> {
			return "";
		};

		// create map
		
		final SelfOrganizingMap<ColorVector, String> map = new SelfOrganizingMap<>(
				colorFactory, stringFactory);

		// define file format for data sets
		
		final PresetInputFactory<SelfOrganizingMap<ColorVector, String>.Node> colorParser = line -> {
			String[] tokens = line.split(",");
			return map.new Node(new ColorVector(
					Integer.parseInt(tokens[0].trim()), 
					Integer.parseInt(tokens[1].trim()), 
					Integer.parseInt(tokens[2].trim())), 
					tokens[3]);
		};

		// create view and connect it to the map
		
		final ColorView view = new ColorView();
		final boolean[] added = new boolean[] { false };
		
		final Runnable updateView = () -> {
			view.setTitle("" + map.age + "/" + map.maxAge + " with " + map.dataSet.size() + " entries.");
			view.updateMap(map);
		};

		// define a click event and load new dataset on click -- even if the old set is being studied
		
		view.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(!added[0]) {
					map.loadDataSetFrom("testing.set", colorParser);
					added[0] = true;
				}						
			}
		});

		map.onFinishedOneStep = updateView;
		map.onFinishedWholeDataSet = updateView;
		map.onLabellingFinished = updateView;

		// enter the training set
		
		map.loadDataSetFrom("training.set", colorParser);
	}
}
