package com.gavrilovmiroslav.awsom.examples.colors;

import java.awt.Color;

import com.gavrilovmiroslav.awsom.Vector;

public class ColorVector extends Vector<ColorVector> {
	public double x, y, z;
	
	public ColorVector() {
		x = y = z = 0;
	}
	
	public ColorVector(int x, int y, int z) {
		this.x = (double)x / 255.0;
		this.y = (double)y / 255.0;
		this.z = (double)z / 255.0;
	}

	public ColorVector(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}	

	@Override
	public double diff(ColorVector v) {
		double rs = this.x - v.x;
		rs *= rs;

		double gs = this.y - v.y;
		gs *= gs;

		double bs = this.z - v.z;
		bs *= bs;

		return Math.sqrt(rs + gs + bs);
	}

	@Override
	public void assimilate(ColorVector input, double strength) {
		this.x = this.x + (input.x - this.x) * strength;
		this.y = this.y + (input.y - this.y) * strength;
		this.z = this.z + (input.z - this.z) * strength;
	}
	
	public static double[] _rgb = new double[] { 0, 0, 0 }, 
						   _lab = new double[] { 0, 0, 0 };
	
	public Color toRGBColor() {
		double xx, yy, zz;
		int r, g, b;
		
		xx = x * 255;
		if(xx < 0) xx = 0;
		if(xx > 255) xx = 255;
		r = (int)xx;
		
		yy= y * 255;
		if(yy < 0) yy = 0;
		if(yy > 255) yy = 255;
		g = (int)yy;
		
		zz = z * 255;
		if(zz < 0) zz = 0;
		if(zz > 255) zz = 255;
		b = (int)zz;
		
		return new Color(r, g, b);
	}
}
