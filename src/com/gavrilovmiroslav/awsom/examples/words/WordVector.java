package com.gavrilovmiroslav.awsom.examples.words;

import java.util.Arrays;
import java.util.Random;
import java.util.stream.Collectors;

import com.gavrilovmiroslav.awsom.Vector;

public class WordVector extends Vector<WordVector> {

	public double[] letters = new double[20];

	@Override
	public double diff(WordVector other) {
		double distance = 0;
		for (int i = 0; i < 20; i++) {
			distance += Math.abs(this.letters[i] - other.letters[i]);
		}
		return distance;		
	}	

	public int levenshteinDistance(String s0, String s1) {                          
	    int len0 = s0.length() + 1;                                                     
	    int len1 = s1.length() + 1;                                                     
	 
	    // the array of distances                                                       
	    int[] cost = new int[len0];                                                     
	    int[] newcost = new int[len0];                                                  
	 
	    // initial cost of skipping prefix in String s0                                 
	    for (int i = 0; i < len0; i++) cost[i] = i;                                     
	 
	    // dynamically computing the array of distances                                  
	 
	    // transformation cost for each letter in s1                                    
	    for (int j = 1; j < len1; j++) {                                                
	        // initial cost of skipping prefix in String s1                             
	        newcost[0] = j;                                                             
	 
	        // transformation cost for each letter in s0                                
	        for(int i = 1; i < len0; i++) {                                             
	            // matching current letters in both strings                             
	            int match = (s0.charAt(i - 1) == s1.charAt(j - 1)) ? 0 : 1;             
	 
	            // computing cost for each transformation                               
	            int cost_replace = cost[i - 1] + match;                                 
	            int cost_insert  = cost[i] + 1;                                         
	            int cost_delete  = newcost[i - 1] + 1;                                  
	 
	            // keep minimum cost                                                    
	            newcost[i] = Math.min(Math.min(cost_insert, cost_delete), cost_replace);
	        }                                                                           
	 
	        // swap cost/newcost arrays                                                 
	        int[] swap = cost; cost = newcost; newcost = swap;                          
	    }                                                                               
	 
	    // the distance is the cost for transforming all letters in both strings        
	    return cost[len0 - 1];                                                          
	}

	@Override
	public void assimilate(WordVector other, double strength) {
		for (int i = 0; i < 20; i++) {
			this.letters[i] += (other.letters[i] - this.letters[i]) * strength;
		}
	}

	public static Random random = new Random();

	public static WordVector createRandom() {
		final WordVector result = new WordVector();
		for(int i = 0; i < 20; i++) {
			result.letters[i] = random.nextInt(30);
		}
		
		return result;
	}
	
	public static WordVector createSpecific(String word) {		
		final WordVector result = new WordVector();
		String token = Arrays.stream(word.toUpperCase().split(" ")).collect(Collectors.joining(""));
		
		int length = token.length();
		for(int i = 0; i < 20; i++) {
			if(i < length)
				result.letters[i] = (int)token.charAt(i) - 64;
			else 
				result.letters[i] = 0;
		}
		
		return result;
	}
	
	public String toString() {
		String result = "";
		for(int i = 0; i < 20; i++) {
			if(this.letters[i] != 0)
				result += (char)((int)this.letters[i] + 64);
		}
		return result;
	}
}
